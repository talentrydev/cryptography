# Cryptography

This is a thin library for encrypting and decrypting strings and generating hashes.

## Installing

* Run:

```
composer require talentrydev/cryptography
```

## Usage example

### Encryption / decryption

```
$factory = new \Talentry\Cryptography\Factory\EncryptionServiceFactory();
$encryptionKey = $factory->generateEncryptionKey();
$service = $factory->generateEncryptionService($encryptionKey);
$encryptedString = $service->encrypt('secret');
$decryptedString = $service->decrypt($encryptedString);
```

WARNING: `encrypt` method is not idempotent. Calling it multiple times with the same input will return different outputs.
The only guarantee is that decrypting the encrypted string will always return the original (unencrypted) string.

### Generate hash

```
$factory = new \Talentry\Cryptography\Factory\HashGeneratorFactory();
$hash = $factory->generate('input');
```

## Development

- Install dependencies: `make deps`
- Run tests: `make test`
