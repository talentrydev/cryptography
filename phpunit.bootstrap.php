<?php

require_once __DIR__ . '/vendor/autoload.php';

use Talentry\ErrorHandling\Factory\ErrorHandlerFactory;
use Talentry\ErrorHandling\Enum\Severity;

(new ErrorHandlerFactory())->generate()->startHandling(Severity::WARNING);
