<?php

declare(strict_types=1);

namespace Talentry\Cryptography\Encryption;

interface EncryptionService
{
    /**
     * WARNING: this method is not idempotent.
     * Calling it multiple times with the same input will return different outputs.
     * The only guarantee is that decrypting the encrypted string will always return the original (unencrypted) string.
     */
    public function encrypt(string $input): string;
    public function decrypt(string $input): string;
}
