<?php

declare(strict_types=1);

namespace Talentry\Cryptography\Encryption\Sodium;

use InvalidArgumentException;
use Talentry\Cryptography\Encryption\EncryptionService;
use Talentry\ErrorHandling\Error\Warning;

class SodiumEncryptionService implements EncryptionService
{
    public function __construct(
        private readonly string $encryptionKey,
    ) {
    }

    public function encrypt(string $input): string
    {
        $nonce = random_bytes(SODIUM_CRYPTO_SECRETBOX_NONCEBYTES);
        $encryptedString = sodium_crypto_secretbox($input, $nonce, $this->encryptionKey);

        return bin2hex($nonce . $encryptedString);
    }

    public function decrypt(string $input): string
    {
        try {
            $rawInput = hex2bin($input);
        } catch (Warning) {
            $rawInput = false;
        }

        $errorMessage = sprintf('Can\'t decrypt the provided input. Was it encrypted using %s?', __CLASS__);
        if ($rawInput === false) {
            throw new InvalidArgumentException($errorMessage);
        }
        $nonce = substr($rawInput, 0, SODIUM_CRYPTO_SECRETBOX_NONCEBYTES);
        $encryptedString = substr($rawInput, SODIUM_CRYPTO_SECRETBOX_NONCEBYTES);

        if (strlen($nonce) < SODIUM_CRYPTO_SECRETBOX_NONCEBYTES) {
            throw new InvalidArgumentException($errorMessage);
        }

        $decryptedString = sodium_crypto_secretbox_open($encryptedString, $nonce, $this->encryptionKey);

        if ($decryptedString === false) {
            throw new InvalidArgumentException('Can\'t decrypt the provided input. Did the encryption key change?');
        }

        return $decryptedString;
    }
}
