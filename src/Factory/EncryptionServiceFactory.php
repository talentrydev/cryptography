<?php

declare(strict_types=1);

namespace Talentry\Cryptography\Factory;

use InvalidArgumentException;
use Talentry\Cryptography\Encryption\EncryptionService;
use Talentry\Cryptography\Encryption\Sodium\SodiumEncryptionService;
use Talentry\ErrorHandling\Error\Warning;

class EncryptionServiceFactory
{
    public function generateEncryptionService(?string $encryptionKey = null): EncryptionService
    {
        if ($encryptionKey === null) {
            $encryptionKey = sodium_crypto_secretbox_keygen();
        } else {
            try {
                $encryptionKey = hex2bin($encryptionKey);
            } catch (Warning) {
                $encryptionKey = false;
            }

            if ($encryptionKey === false) {
                throw new InvalidArgumentException('Invalid encryption key provided');
            }
        }

        return new SodiumEncryptionService($encryptionKey);
    }

    public function generateEncryptionKey(): string
    {
        return bin2hex(sodium_crypto_secretbox_keygen());
    }
}
