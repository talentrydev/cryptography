<?php

declare(strict_types=1);

namespace Talentry\Cryptography\Factory;

use Talentry\Cryptography\Hashing\HashGenerator;
use Talentry\Cryptography\Hashing\Sodium\SodiumHashGenerator;

class HashGeneratorFactory
{
    private const DEFAULT_HASH_LENGTH = 32;

    public function generate(string $key, int $hashLength = self::DEFAULT_HASH_LENGTH): HashGenerator
    {
        return new SodiumHashGenerator($key, $hashLength);
    }
}
