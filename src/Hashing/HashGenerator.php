<?php

declare(strict_types=1);

namespace Talentry\Cryptography\Hashing;

interface HashGenerator
{
    public function generateUrlSafeHash(string $input): string;
    public function generateBase64Hash(string $input): string;
}
