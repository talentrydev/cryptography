<?php

declare(strict_types=1);

namespace Talentry\Cryptography\Hashing\Sodium;

use InvalidArgumentException;
use Talentry\Cryptography\Hashing\HashGenerator;

class SodiumHashGenerator implements HashGenerator
{
    public function __construct(
        private readonly string $key,
        private readonly int $hashLength,
    ) {
        $this->validateKey($this->key);
    }

    public function generateUrlSafeHash(string $input): string
    {
        return rtrim(str_replace(['+', '/'], ['_', '-'], $this->generateBase64Hash($input)), '=');
    }

    public function generateBase64Hash(string $input): string
    {
        return base64_encode($this->generateHash($input));
    }

    private function generateHash(string $input): string
    {
        return sodium_crypto_generichash($input, $this->key, $this->hashLength);
    }

    private function validateKey(string $key): void
    {
        $length = strlen($key);
        if ($length < SODIUM_CRYPTO_GENERICHASH_KEYBYTES_MIN || $length > SODIUM_CRYPTO_GENERICHASH_KEYBYTES_MAX) {
            throw new InvalidArgumentException(
                sprintf(
                    'key must be between %s and %s characters long',
                    SODIUM_CRYPTO_GENERICHASH_KEYBYTES_MIN,
                    SODIUM_CRYPTO_GENERICHASH_KEYBYTES_MAX
                )
            );
        }
    }
}
