<?php

declare(strict_types=1);

namespace Talentry\Cryptography\Tests\Encryption\Sodium;

use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use Talentry\Cryptography\Encryption\EncryptionService;
use Talentry\Cryptography\Encryption\Sodium\SodiumEncryptionService;
use Talentry\Cryptography\Factory\EncryptionServiceFactory;
use PHPUnit\Framework\Attributes\DataProvider;

class SodiumEncryptionServiceTest extends TestCase
{
    private EncryptionService $encryptionService;

    protected function setUp(): void
    {
        $this->encryptionService = (new EncryptionServiceFactory())->generateEncryptionService();
    }

    public function testEncryptionAndDecryption(): void
    {
        $secret = 'secret';
        $encryptedSecret = $this->encryptionService->encrypt($secret);
        $decryptedSecret = $this->encryptionService->decrypt($encryptedSecret);
        self::assertSame($secret, $decryptedSecret);
    }

    #[DataProvider('invalidInputDataProvider')]
    public function testDecryptInvalidInput(string $input): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage(sprintf(
            'Can\'t decrypt the provided input. Was it encrypted using %s?',
            SodiumEncryptionService::class
        ));
        $this->encryptionService->decrypt($input);
    }

    public static function invalidInputDataProvider(): array
    {
        return [
            ['foo'],
            [bin2hex('foo')],
        ];
    }

    public function testDecryptWithInvalidEncryptionKey(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Can\'t decrypt the provided input. Did the encryption key change?');
        $encrypted = $this->encryptionService->encrypt('foo');
        (new EncryptionServiceFactory())->generateEncryptionService()->decrypt($encrypted);
    }
}
