<?php

declare(strict_types=1);

namespace Talentry\Cryptography\Tests\Factory;

use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use Talentry\Cryptography\Encryption\Sodium\SodiumEncryptionService;
use Talentry\Cryptography\Factory\EncryptionServiceFactory;

class EncryptionServiceFactoryTest extends TestCase
{
    private EncryptionServiceFactory $factory;
    private string $validEncryptionKey;
    private string $invalidEncryptionKey = 'foo';

    protected function setUp(): void
    {
        $this->factory = new EncryptionServiceFactory();
        $this->validEncryptionKey = $this->factory->generateEncryptionKey();
    }

    public function testGenerateEncryptionServiceWithoutEncryptionKey(): void
    {
        $encryptionService = $this->factory->generateEncryptionService();
        self::assertInstanceOf(SodiumEncryptionService::class, $encryptionService);
    }

    public function testGenerateEncryptionServiceWithValidEncryptionKey(): void
    {
        $encryptionService = $this->factory->generateEncryptionService($this->validEncryptionKey);
        self::assertInstanceOf(SodiumEncryptionService::class, $encryptionService);
    }

    public function testGenerateEncryptionServiceWithInvalidEncryptionKey(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Invalid encryption key provided');
        $this->factory->generateEncryptionService($this->invalidEncryptionKey);
    }
}
