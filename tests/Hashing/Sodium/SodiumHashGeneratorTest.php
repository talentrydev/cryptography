<?php

declare(strict_types=1);

namespace Talentry\Cryptography\Tests\Hashing\Sodium;

use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use Talentry\Cryptography\Hashing\Sodium\SodiumHashGenerator;

class SodiumHashGeneratorTest extends TestCase
{
    private SodiumHashGenerator $hashingService;
    private int $hashLength;

    protected function setUp(): void
    {
        $this->hashLength = 32;
        $this->hashingService = new SodiumHashGenerator(str_repeat('x', 16), $this->hashLength);
    }

    public function testBase64(): void
    {
        $hash = $this->hashingService->generateBase64Hash('95f4033d5');
        self::assertSame('14p/lXNn+HYpc5nH+hDJmmEnuYHx+j1BsKnhjP/fasM=', $hash);
    }

    public function testSameStringGeneratesSameHash(): void
    {
        $input = 'input';
        $hash1 = $this->hashingService->generateBase64Hash($input);
        $hash2 = $this->hashingService->generateBase64Hash($input);
        self::assertSame($hash1, $hash2);
    }

    public function testDifferentStringsGenerateDifferentHashes(): void
    {
        $hash1 = $this->hashingService->generateBase64Hash('a');
        $hash2 = $this->hashingService->generateBase64Hash('b');
        self::assertNotSame($hash1, $hash2);
    }

    public function testUrlSafeHash(): void
    {
        $hash = $this->hashingService->generateUrlSafeHash('fcf50c28b138ZZZ');

        // Would be 8gm5xH9xPG/hdaA5SK+csRcsWRyXozKF2fmjtjC9Khc without URL replacements.
        self::assertEquals('8gm5xH9xPG-hdaA5SK_csRcsWRyXozKF2fmjtjC9Khc', $hash);
    }

    public function testHashLength(): void
    {
        self::assertSame(44, strlen($this->hashingService->generateBase64Hash('')));
    }

    public function testWithInvalidKey(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('key must be between 16 and 64 characters long');
        new SodiumHashGenerator('foo', $this->hashLength);
    }
}
